package com.adapp.adapp_pos_iam.utils;

import com.adapp.adapp_pos_models.audit.AuditMessage;
import com.adapp.adapp_pos_models.enums.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class IAMKafkaAuditSender<T> {

    public static final String TOPIC = "audit";
    KafkaTemplate<String, AuditMessage<T>> kafkaTemplate;


    @Autowired
    public IAMKafkaAuditSender(KafkaTemplate<String, AuditMessage<T>> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(Resource auditResource, String objectId, String accountId, String identifierName, String identifier) {
        AuditMessage<T> auditMessage = new AuditMessage.Builder<T>()
                .forResource(auditResource)
                .withAction(AuditMessage.AuditAction.CREATE)
                .withAuditedId(objectId)
                .forAccountId(accountId)
                .withIdentifier(identifierName, identifier)
                .build();
        this.kafkaTemplate.send(TOPIC, auditMessage);
    }

    public void send(
            Resource auditResource,
            String objectId,
            String accountId, String identifierName,
            String identifier,
            T before,
            T after) {
        AuditMessage<T> auditMessage = new AuditMessage.Builder<T>()
                .forResource(auditResource)
                .withAction(AuditMessage.AuditAction.UPDATE)
                .beforeChange(before)
                .afterChange(after)
                .withAuditedId(objectId)
                .forAccountId(accountId)
                .withIdentifier(identifierName, identifier)
                .build();
        this.kafkaTemplate.send(TOPIC, auditMessage);
    }

    public void send(Resource auditResource, String accountId, String identifierName, String identifier) {
        AuditMessage<T> auditMessage = new AuditMessage.Builder<T>()
                .forResource(auditResource)
                .withAction(AuditMessage.AuditAction.DELETE)
                .withAuditedId("deleted")
                .forAccountId(accountId)
                .withIdentifier(identifierName, identifier)
                .build();
        this.kafkaTemplate.send(TOPIC, auditMessage);
    }
}
