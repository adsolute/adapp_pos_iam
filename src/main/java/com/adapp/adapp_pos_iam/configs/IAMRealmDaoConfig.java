package com.adapp.adapp_pos_iam.configs;

import com.adapp.security.dao.AccountDao;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.dao.UserDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IAMRealmDaoConfig {

	@Bean
	public RoleDao createRoleDao() {
		return new RoleDao<>();
	}

	@Bean
	public UserDao createUserDao() {
		return new UserDao();
	}

	@Bean
	public AccountDao createAccountDao() {
		return new AccountDao();
	}
}
