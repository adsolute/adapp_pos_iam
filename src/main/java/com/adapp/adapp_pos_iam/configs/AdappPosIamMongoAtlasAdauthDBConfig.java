package com.adapp.adapp_pos_iam.configs;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@RefreshScope
@Configuration
@ConditionalOnExpression("'${spring.data.mongodb.host}'.contains('mongodb+srv')")
public class AdappPosIamMongoAtlasAdauthDBConfig {
    @Value("${spring.data.mongodb.host}")
    private String mongoHost;
    @Value("${spring.data.mongodb.database}")
    private String dbName;

    private ConnectionString createConnectionString() {
        return new ConnectionString(mongoHost);
    }

    private MongoClientSettings buildMongoClientSettings() {
        return MongoClientSettings
                .builder()
                .applyConnectionString(createConnectionString())
                .build();
    }

    public SimpleMongoClientDatabaseFactory createMongoDbFactory() {

        return new SimpleMongoClientDatabaseFactory(
                MongoClients.create(buildMongoClientSettings()),
                dbName
        );
    }

    @Bean(name = "adauthMongoTemplate")
    public MongoTemplate createMongoTemplate() {
        return new MongoTemplate(createMongoDbFactory());
    }
}
