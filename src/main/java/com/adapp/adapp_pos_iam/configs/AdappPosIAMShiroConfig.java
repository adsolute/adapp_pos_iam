package com.adapp.adapp_pos_iam.configs;

import com.adapp.security.filter.FormAPIAuthenticationFilter;
import com.adapp.security.realms.UserCredentialsRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.servlet.Filter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class AdappPosIAMShiroConfig {

	public static final long SIX_HOURS_IN_MILLISECONDS = 21600000L;
	@Value("${REDIS_HOST}")
	private String redisHost;
	@Value("${REDIS_PORT}")
	private int redisPort;
	@Autowired
	Environment environment;

	@Bean
	public Realm createUserCredentialsRealm() {
		UserCredentialsRealm userCredentialsRealm = new UserCredentialsRealm();
		userCredentialsRealm.setCredentialsMatcher(createHashedCreadentialsMatcher());
		return userCredentialsRealm;
	}

	@Bean
	public ShiroFilterFactoryBean createShiroFilterFactoryBean() {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setFilterChainDefinitionMap(createShiroFilterChainDefinition().getFilterChainMap());
		shiroFilterFactoryBean.setSecurityManager(createDefaultWebSecurityManager());
		shiroFilterFactoryBean.setFilters(createFilterMap());
		return shiroFilterFactoryBean;
	}

	private HashedCredentialsMatcher createHashedCreadentialsMatcher() {
		HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
		hashedCredentialsMatcher.setHashAlgorithmName("SHA-512");
		hashedCredentialsMatcher.setStoredCredentialsHexEncoded(false);
		hashedCredentialsMatcher.setHashIterations(256);
		return hashedCredentialsMatcher;
	}

	private SimpleCookie createSimpleCookie() {
		SimpleCookie simpleCookie = new SimpleCookie();
		simpleCookie.setName("JSESSIONID");
		simpleCookie.setPath("/");
//		if (environment.getActiveProfiles().length > 0) {
//			if("prod".equals(environment.getActiveProfiles()[0])) {
//				simpleCookie.setSecure(true);
//				simpleCookie.setComment(";SameSite=None");
//			}
//		}

		return simpleCookie;
	}

	private DefaultWebSessionManager createDefaultWebSessionManager() {
		DefaultWebSessionManager defaultWebSessionManager = new DefaultWebSessionManager();
		defaultWebSessionManager.setSessionIdCookie(createSimpleCookie());
		defaultWebSessionManager.setSessionDAO(createRedisSessionDao());
		defaultWebSessionManager.setGlobalSessionTimeout(SIX_HOURS_IN_MILLISECONDS);

		return defaultWebSessionManager;
	}

	private DefaultWebSecurityManager createDefaultWebSecurityManager() {
		DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
		defaultWebSecurityManager.setRealms(Collections.singletonList(createUserCredentialsRealm()));
		defaultWebSecurityManager.setSessionManager(createDefaultWebSessionManager());
		defaultWebSecurityManager.setCacheManager(createRedisCacheManager());
		return defaultWebSecurityManager;
	}

	private ShiroFilterChainDefinition createShiroFilterChainDefinition() {
		DefaultShiroFilterChainDefinition defaultShiroFilterChainDefinition = new DefaultShiroFilterChainDefinition();
		defaultShiroFilterChainDefinition.addPathDefinition("/auth/login", "anon");
		defaultShiroFilterChainDefinition.addPathDefinition("/auth/check", "anon");
		defaultShiroFilterChainDefinition.addPathDefinition("/**", "auth");
		return defaultShiroFilterChainDefinition;
	}

	private Map<String, Filter> createFilterMap() {
		Map<String, Filter> filterMap = new HashMap<>();
		filterMap.put("auth", new FormAPIAuthenticationFilter());
		return filterMap;
	}

	private RedisCacheManager createRedisCacheManager() {
		RedisCacheManager redisCacheManager = new RedisCacheManager();
		redisCacheManager.setRedisManager(createRedisManager());
		return redisCacheManager;
	}

	private RedisSessionDAO createRedisSessionDao() {
		RedisSessionDAO redisSessionDAO =  new RedisSessionDAO();
		redisSessionDAO.setRedisManager(createRedisManager());
		return redisSessionDAO;
	}

	private RedisManager createRedisManager() {
		String host = createHost();
		RedisManager redisManager = new RedisManager();
		redisManager.setHost(host);
		return redisManager;
	}

	private String createHost() {
		return String.format("%s:%s", redisHost, redisPort);
	}
}
