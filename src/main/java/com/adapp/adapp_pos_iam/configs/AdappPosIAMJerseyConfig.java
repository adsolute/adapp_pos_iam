package com.adapp.adapp_pos_iam.configs;

import com.adapp.adapp_pos_iam.controllers.AuthController;
import com.adapp.adapp_pos_iam.controllers.RoleController;
import com.adapp.adapp_pos_iam.controllers.UserAccountController;
import com.adapp.adapp_pos_iam.controllers.UserController;
import com.adapp.adapp_pos_models.exceptions.ServerExceptionMapper;
import com.adapp.adapp_pos_models.utils.CORSFilter;
import com.adapp.adapp_pos_models.utils.JacksonMapperContextResolver;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdappPosIAMJerseyConfig extends ResourceConfig {

	public AdappPosIAMJerseyConfig(){
		property( ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true );
		register(CORSFilter.class);
		register(JacksonMapperContextResolver.class);
		register(ServerExceptionMapper.class);
//		packages( "com.adapp.adapp_security.controllers");
		register(AuthController.class);
		register(RoleController.class);
		register(UserAccountController.class);
		register(UserController.class);
	}
}
