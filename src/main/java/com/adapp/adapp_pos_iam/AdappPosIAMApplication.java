package com.adapp.adapp_pos_iam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath:*-config.xml"})
public class AdappPosIAMApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdappPosIAMApplication.class, args);
	}

}
