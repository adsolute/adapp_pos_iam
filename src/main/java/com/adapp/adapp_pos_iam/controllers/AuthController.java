package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.dao.UserAccountDao;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.security.Credentials;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Path("auth")
@Component
public class AuthController {

    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);
    public static final String INVALID_CREDENTIALS = "Invalid Credentials";

    UserAccountDao userAccountDao;

    @Autowired
    public AuthController(UserAccountDao userAccountDao) {
        this.userAccountDao = userAccountDao;
    }

    @POST
    @Path("login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response login (Credentials credentials) {
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(
                credentials.getUsername(),
                credentials.getPassword().toCharArray()
        );
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(usernamePasswordToken);
            UserPrincipal userPrincipal = (UserPrincipal) subject.getPrincipal();
            UserAccount userAccount = userAccountDao.retrieve(userPrincipal.getAccountId());
            if (userAccount != null) {
                SecurityUtils.getSubject().isAuthenticated();
                return Response.ok(userAccount).build();
            } else  {
                subject.logout();
                return Response
                        .status(UNAUTHORIZED)
                        .entity(String.format("%s authentication was successful but has no authorized account(s) to proceed. Please Contact Admin", userPrincipal.getUsername()))
                        .build();
            }
        } catch (IncorrectCredentialsException incorrectCredentialsException) {
            LOG.error(incorrectCredentialsException.getMessage(), incorrectCredentialsException);
            return Response.status(UNAUTHORIZED).entity(INVALID_CREDENTIALS).build();
        } catch (Exception e) {
            throw new ServerException(e.getMessage(), e);
        }
    }

    @GET
    @Path("check")
    public Response checkAuthentication() {
        if(SecurityUtils.getSubject().isAuthenticated()) {
            return Response.ok().build();
        }

        return Response
                .status(UNAUTHORIZED)
                .build();
    }

    @DELETE
    @Path("logout")
    public Response logout() {
        try{
            SecurityUtils.getSubject().logout();
            return Response.ok().build();
        } catch (Exception e) {
            throw new ServerException(e.getMessage(), e);
        }
    }

}
