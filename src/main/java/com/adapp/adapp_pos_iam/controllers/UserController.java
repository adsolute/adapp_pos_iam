package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.services.UserService;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.models.Permission;
import com.adapp.security.models.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.security.models.Permission.Action.CREATE;
import static com.adapp.security.models.Permission.Action.UPDATE;

@Path("users")
@Component
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GET
	@Path("{username}")
	public Response getUserByAccountId(@PathParam("username") String username, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			return Response.ok(userService.getUserDTOByUsername(username)).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(Response.Status.NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response save(User user, @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s",
					Resource.ACCOUNTS,
					CREATE));
			return Response.ok(userService.save(user)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response update(User user, @QueryParam("branch-id") String branchId) {

		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s",
					Resource.ACCOUNTS,
					UPDATE));
			userService.update(user);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
