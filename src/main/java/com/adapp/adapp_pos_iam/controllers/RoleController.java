package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.services.RoleService;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.dao.BaseDao.QueryParameters;
import com.adapp.security.models.Role;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.adapp_pos_models.enums.Resource.ROLES;
import static com.adapp.security.models.Permission.Action.READ;
import static com.adapp.security.models.Permission.Action.UPDATE;
import static javax.ws.rs.core.Response.Status.*;

@Path("roles")
@Component
public class RoleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);

	RoleService roleService;

	@Autowired
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}

	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllRoles() {
		try {
			return Response.ok(roleService.getAllRoles()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response save(Role<Resource> resourceRole, @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "ROLES:READ");
			roleService.save(resourceRole);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("query")
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRolePagedResultSet(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "ROLES:READ");
			return Response.ok(roleService.getRolePagedResultSetBy(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("user/role")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getActiveUserRole(@QueryParam("role-id") String roleId) {
		try {
			Role<Resource> resourceRole = roleService.getActiveUserRole(roleId);
			if (resourceRole == null) {
				return Response.status(NOT_FOUND).entity(String.format("Role with id %s Not Found", roleId)).build();
			}
			return Response.ok(resourceRole).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(UNAUTHORIZED)
					.entity(String.format("Active user has no granted role with id:%s", roleId))
					.build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
	@GET
	@Path("role")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRole(@QueryParam("id") String id,@DefaultValue("*") @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", ROLES, READ));
			Role role = roleService.getRole(id);
			if (role == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(String.format("Role with ObjectId:%s, Not Found", id)).build();
			}
			return Response.ok(role).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(unauthorizedException.getMessage())
					.build();
		} catch (IllegalArgumentException illegalArgumentException){
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(String.format("Bad Request, Illegal Role ObjectId:%s", id))
					.build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@QueryParam("branch-id") @DefaultValue("*") String branchId, Role<Resource> resourceRole) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", ROLES, UPDATE));
			if (resourceRole.getId() == null) {
				return Response.status(BAD_REQUEST).entity("Role Object Id cannot be null").build();
			}

			roleService.save(resourceRole);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
