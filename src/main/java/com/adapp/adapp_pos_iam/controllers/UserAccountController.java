package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.services.UserAccountService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.dao.BaseDao.QueryParameters;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.adapp_pos_models.enums.Resource.ACCOUNTS;
import static com.adapp.security.models.Permission.Action.*;
import static javax.ws.rs.core.Response.Status.*;

@Path("user-accounts")
@Component
public class UserAccountController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserAccountController.class);

	UserAccountService userAccountService;

	@Autowired
	public UserAccountController(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response save(@QueryParam("branch-id") @DefaultValue("*") String branchId, UserAccount userAccount) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", ACCOUNTS, CREATE));
			return Response.ok(userAccountService.save(userAccount)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response update(@QueryParam("branch-id") @DefaultValue("*") String branchId, UserAccount userAccount) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", ACCOUNTS, UPDATE));
			return Response.ok(userAccountService.update(userAccount)).build();
		} catch (BadRequestException badRequestException) {
			return Response.status(BAD_REQUEST).entity(badRequestException.getMessage()).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("query")
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getPagedResultSet(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "ACCOUNTS:READ");
			return Response.ok(userAccountService.getUserAccountDTOPagedResultSetBy(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAccountById(@QueryParam("account-id") String accountId) {
		try {
			return Response.ok(userAccountService.getUserAccountByIdOrSubject(accountId)).build();
		}  catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("account")
	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAccountByAccountId(@QueryParam("account-id") String accountId, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", ACCOUNTS, READ));
			return Response.ok(userAccountService.getUserAccountByAccountId(accountId)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (BadRequestException badRequestException) {
			return Response.status(BAD_REQUEST).entity(badRequestException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@DELETE
	@Path("{username}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response delete(@PathParam("username") @NotNull String username, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "ACCOUNTS:DELETE");
			userAccountService.delete(username);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("riders")
	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRiderAccounts(@QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			return Response.ok(userAccountService.getRiderUserAccounts(branchId)).build();
		}  catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
