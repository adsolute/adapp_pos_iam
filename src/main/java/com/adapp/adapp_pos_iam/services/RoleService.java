package com.adapp.adapp_pos_iam.services;

import com.adapp.adapp_pos_iam.dao.UserAccountDao;
import com.adapp.adapp_pos_iam.utils.IAMKafkaAuditSender;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.utils.IdGenerationUtility;
import com.adapp.security.dao.BaseDao.PagedResultSet;
import com.adapp.security.dao.BaseDao.QueryParameters;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.models.GrantedRole;
import com.adapp.security.models.Role;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

	public static final String NAME = "Name";
	RoleDao<Resource> roleDao;

	UserAccountDao userAccountDao;

	IAMKafkaAuditSender<Role<Resource>> iamKafkaAuditSender;

	@Autowired
	public RoleService(RoleDao<Resource> roleDao, UserAccountDao userAccountDao, IAMKafkaAuditSender<Role<Resource>> iamKafkaAuditSender) {
		this.roleDao = roleDao;
		this.userAccountDao = userAccountDao;
		this.iamKafkaAuditSender = iamKafkaAuditSender;
	}

	public List<Role<Resource>> getAllRoles() {
		return roleDao.getAllRoles();
	}

	public void save(Role<Resource> role) {
		DateTime now = new DateTime();
		role.setUpdateDate(now);
		if(role.getId() == null) {
			role.setCreateDate(now);
			role.setRoleId(IdGenerationUtility.generateId(roleDao::exists));
			Role<Resource> savedRole = roleDao.save(role);
			this.iamKafkaAuditSender.send(Resource.ROLES, savedRole.getId(), null, NAME, savedRole.getName());
		} else {
			Role<Resource> roleBeforeUpdate = roleDao.findAndReplace(role);
			this.iamKafkaAuditSender.send(Resource.ROLES, role.getId(), null, NAME, role.getName(), roleBeforeUpdate, role);
		}

	}

	public PagedResultSet<Role<Resource>> getRolePagedResultSetBy(QueryParameters queryParameters) {
		return roleDao.getRolePagedResultSetBy(queryParameters);
	}

	public Role<Resource> getActiveUserRole(String roleId) {
		Subject subject = SecurityUtils.getSubject();
		UserPrincipal userPrincipal = (UserPrincipal) subject.getPrincipal();
		UserAccount userAccount = userAccountDao.retrieve(userPrincipal.getAccountId());

		if (userAccount == null) {
			throw new UnauthorizedException("Active User has no UserAccount to continue");
		}

		Optional<GrantedRole> optionalGrantedRole = userAccount.getGrantedRoles()
				.stream()
				.filter(grantedRole -> grantedRole.getRoleId().equals(roleId))
				.findFirst();

		if (optionalGrantedRole.isPresent()) {
			return roleDao.getRole(roleId);
		} else {
			throw new UnauthorizedException(String.format("Active User has no granted role with id:%s", roleId));
		}
	}

	public Role<Resource> getRole(String id) {
		return roleDao.getRoleByObjectId(id);
	}
}
