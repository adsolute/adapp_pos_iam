package com.adapp.adapp_pos_iam.services;

import com.adapp.adapp_pos_models.security.UserDTO;
import com.adapp.adapp_pos_models.utils.IdGenerationUtility;
import com.adapp.security.dao.UserDao;
import com.adapp.security.models.User;
import com.adapp.security.utils.HashingUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;

@Service
public class UserService {

	UserDao userDao;

	@Autowired
	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}

	public User save(User user) {
		if (userDao.exists(user.getUsername())) {
			throw new IllegalArgumentException("Username Already Exist");
		}

		if (user.getUsername() == null) {
			throw new IllegalArgumentException("Username cannot be null");
		}

		if (user.getPassword() == null) {
			throw new IllegalArgumentException("Password cannot be null");
		}

		user.setUserId(IdGenerationUtility.generateId(userDao::existsUserId));
		user.setSalt(HashingUtility.generateSalt());
		user.setPassword(HashingUtility.hashPassword(user.getPassword(), user.getSalt()));
		return userDao.save(user);
	}


	public void update(User user) {
		if (user.getUsername() == null) {
			throw new IllegalArgumentException("Username cannot be null");
		}

		if (user.getId() == null) {
			throw new IllegalArgumentException("User's object id cannot be null for update");
		}

		User userToBeUpdated = userDao.getByUsername(user.getUsername());
		userToBeUpdated.setAccountId(user.getAccountId());
		userDao.save(userToBeUpdated);
	}

	public UserDTO getUserDTOByUsername(String username) {
		User user = userDao.getByUsername(username);

		if (user == null) {
			throw new NotFoundException(String.format("User with username %s, Not Found", username));
		}

		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setUsername(user.getUsername());
		userDTO.setAccountId(user.getAccountId());

		return userDTO;
	}
}
