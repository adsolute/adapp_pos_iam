package com.adapp.adapp_pos_iam.services;

import com.adapp.adapp_pos_iam.dao.UserAccountDao;
import com.adapp.adapp_pos_iam.utils.IAMKafkaAuditSender;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.profiles.UserAccountDTO;
import com.adapp.adapp_pos_models.utils.IdGenerationUtility;
import com.adapp.security.dao.BaseDao.PagedResultSet;
import com.adapp.security.dao.BaseDao.QueryParameters;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.dao.UserDao;
import com.adapp.security.models.Role;
import com.adapp.security.models.User;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.adapp.adapp_pos_models.enums.Resource.ACCOUNTS;

@Service
public class UserAccountService {

	public static final String EMPTY_NAME = "";
	public static final String NAME = "NAME";
	UserAccountDao userAccountDao;

	UserDao userDao;

	RoleDao<Resource> roleDao;

	IAMKafkaAuditSender<UserAccount> iamKafkaAuditSender;

	@Autowired
	public UserAccountService(UserAccountDao userAccountDao, UserDao userDao, RoleDao<Resource> roleDao, IAMKafkaAuditSender<UserAccount> iamKafkaAuditSender) {
		this.userAccountDao = userAccountDao;
		this.userDao = userDao;
		this.roleDao = roleDao;
		this.iamKafkaAuditSender = iamKafkaAuditSender;
	}

	public UserAccount save(UserAccount userAccount) {
		userAccount.setAccountId(IdGenerationUtility.generateId(userAccountDao::exists));
		UserAccount savedUserAccount = userAccountDao.save(userAccount);
		this.iamKafkaAuditSender.send(ACCOUNTS, savedUserAccount.getId(), null, NAME, userAccount.getPersonalDetails() != null ? userAccount.getPersonalDetails().getLastName() : EMPTY_NAME);
		return savedUserAccount;
	}

	public UserAccount update(UserAccount userAccount) {
		if (userAccount.getAccountId() == null) {
			throw new BadRequestException("Cannot update an user account that has no accountId");
		}
		UserAccount userAccountBeforeUpdate = userAccountDao.findAndReplace(userAccount.getId(), userAccount);
		this.iamKafkaAuditSender
				.send(
						ACCOUNTS,
						userAccount.getId(),
						null, NAME,
						userAccount.getPersonalDetails() != null ? userAccount.getPersonalDetails().getLastName() : EMPTY_NAME,
						userAccountBeforeUpdate,
						userAccount);
		return userAccount;
	}

	public PagedResultSet<UserAccountDTO> getUserAccountDTOPagedResultSetBy(QueryParameters queryParameters) {
		PagedResultSet<User> userPagedResultSet = userDao.getPagedResultSetBy(queryParameters);
		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = new PagedResultSet<>();
		userAccountDTOPagedResultSet.setPage(userPagedResultSet.getPage());
		userAccountDTOPagedResultSet.setSize(userPagedResultSet.getSize());
		userAccountDTOPagedResultSet.setTotalCount(userPagedResultSet.getTotalCount());
		userAccountDTOPagedResultSet.setLimit(queryParameters.getLimit());
		userAccountDTOPagedResultSet.setResults(
				userPagedResultSet.getResults()
					.stream()
					.map(this::buildUserAccountDTO)
					.collect(Collectors.toList())
		);
		return userAccountDTOPagedResultSet;
	}

	private UserAccountDTO buildUserAccountDTO(User user) {
		UserAccountDTO userAccountDTO = new UserAccountDTO();

		UserAccount userAccount = userAccountDao.retrieve(user.getAccountId());
		if (userAccount != null) {
			userAccountDTO.setPhone(userAccount.getContactInformation().getPhoneNumber());
			userAccountDTO.setEmail(userAccount.getContactInformation().getEmailAddress());
			userAccountDTO.setAccountId(userAccount.getAccountId());
		} else {
			userAccountDTO.setAccountId(EMPTY_NAME);
		}
		userAccountDTO.setUsername(user.getUsername());
		return userAccountDTO;
	}

	public UserAccount getUserAccountByIdOrSubject(String accountId) {
		if (accountId == null) {
			accountId = ((UserPrincipal) SecurityUtils.getSubject().getPrincipal()).getAccountId();
		}
		return userAccountDao.retrieve(accountId);
	}

	public UserAccount getUserAccountByAccountId(String accountId) {
		if (accountId == null) {
			throw new BadRequestException("Cannot retrieve a user account with null accountId");
		}
		return userAccountDao.retrieve(accountId);
	}

	public void delete(String username) {
		User user = userDao.getByUsername(username);
		if (user == null) {
			throw new NotFoundException(String.format("Cannot find user to be removed with username, %s", username));
		}
		UserAccount userAccountToRemove = userAccountDao.retrieve(user.getAccountId());
		if (userAccountToRemove != null) {
			userAccountDao.remove(userAccountToRemove);
			iamKafkaAuditSender.send(ACCOUNTS, null, NAME, userAccountToRemove.getPersonalDetails() != null ? userAccountToRemove.getPersonalDetails().getLastName() : EMPTY_NAME);
		}
		userDao.remove(user);
	}

	public List<UserAccount> getRiderUserAccounts(String branchId) {
		QueryParameters queryParameters = new QueryParameters();
		queryParameters.getCriteria().put("otherProperties.forRider", true);
		List<Role<Resource>> riderRoles = roleDao.getRolePagedResultSetBy(queryParameters).getResults();
		if(!riderRoles.isEmpty()) {
			List<String> riderRoleIds = new ArrayList<>();
			riderRoles.stream().forEach(riderRole -> riderRoleIds.add(riderRole.getRoleId()));
			return userAccountDao.getUserAccountsWithRolesInListByBranch(riderRoleIds, branchId);
		} else {
			return new ArrayList<>();
		}
	}
}
