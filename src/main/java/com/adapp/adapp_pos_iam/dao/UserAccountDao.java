package com.adapp.adapp_pos_iam.dao;

import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.security.dao.BaseDao;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserAccountDao extends BaseDao<UserAccount> {

	public boolean exists(String accountId) {
		return super.exists("accountId", accountId, UserAccount.class);
	}

	public UserAccount retrieve(String accountId) {
		Map<String, Object> criteriaMap = new HashMap<>();
		criteriaMap.put("accountId", accountId);
		return super.retrieveOneByCriteriaMap(UserAccount.class, criteriaMap);
	}

	public void remove(UserAccount userAccount) {
		super.remove(userAccount);
	}

	public List<UserAccount> getUserAccountsWithRolesInListByBranch(List<String> roleIds, String branchId) {
		Criteria elemMatchCriteria;
		if("*".equals(branchId)) {
			elemMatchCriteria = new Criteria()
					.andOperator(
							Criteria.where("roleId").in(roleIds));
		} else {
			elemMatchCriteria = new Criteria()
					.andOperator(
							Criteria.where("roleId").in(roleIds),
							Criteria.where("onAccountId").is(branchId));
		}
		QueryParameters queryParameters = new QueryParameters();
		queryParameters.getCustomCriteria().add(Criteria.where("grantedRoles")
				.elemMatch(elemMatchCriteria));
		return super.retrieve(queryParameters, UserAccount.class).getResults();
	}

}
