package com.adapp.adapp_pos_iam.services;

import com.adapp.adapp_pos_iam.dao.UserAccountDao;
import com.adapp.adapp_pos_iam.utils.IAMKafkaAuditSender;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.security.dao.BaseDao.PagedResultSet;
import com.adapp.security.dao.BaseDao.QueryParameters;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.models.GrantedRole;
import com.adapp.security.models.Role;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTest {

	public static final String ROLE_ID = "role id";
	public static final String EXISTING_ROLE_ID = "Existing Role Id";
	public static final String FOR_RIDER = "forRider";
	public static final String ID = "ID";
	@InjectMocks
	RoleService classUnderTest;

	@Mock
	RoleDao<Resource> roleDao;

	@Mock
	UserAccountDao userAccountDao;

	@Mock
	Subject subject;

	@Mock
	IAMKafkaAuditSender<Role<Resource>> iamKafkaAuditSender;

	@BeforeEach
	public void setup() {
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	public void getAllRolesSucceed() {
		List<Role<Resource>> savedRoles = givenThatThereAreSavedRoles();

		when(roleDao.getAllRoles()).thenReturn(savedRoles);
		List<Role<Resource>> roles = classUnderTest.getAllRoles();
		assertTrue(!roles.isEmpty());
		verify(roleDao).getAllRoles();
	}

	private List<Role<Resource>> givenThatThereAreSavedRoles() {
		return Arrays.asList(new Role());
	}

	@Test
	public void getAllRolesSucceedWithEmptyList() {
		List<Role<Resource>> roles = classUnderTest.getAllRoles();
		assertTrue(roles.isEmpty());
		verify(roleDao, times(1)).getAllRoles();
	}

	@Test
	public void saveSucceed() {
		Role<Resource> savedRole = new Role<>();
		savedRole.setName("Saved Role");
		savedRole.setId(ID);
		ArgumentCaptor<Role<Resource>> roleArgumentCaptor = ArgumentCaptor.forClass(Role.class);
		given(roleDao.save(any(Role.class))).willReturn(savedRole);
		classUnderTest.save(new Role<>());
		verify(roleDao).save(roleArgumentCaptor.capture());
		savedRole = roleArgumentCaptor.getValue();
		assertEquals(savedRole.getCreateDate(), savedRole.getUpdateDate());
		verify(iamKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), eq(null), anyString(), anyString());

	}

	@Test
	public void updateSucceed() {
		Role<Resource> roleBeforeUpdate = new Role<>();
		roleBeforeUpdate.setName("Role Before Update");
		Role<Resource> updatedRole = new Role<>();
		updatedRole.setName("Updated Role");
		updatedRole.setId(ID);
		given(roleDao.findAndReplace(any(Role.class))).willReturn(roleBeforeUpdate);

		classUnderTest.save(updatedRole);

		verify(roleDao).findAndReplace(any(Role.class));
		verify(iamKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), eq(null), anyString(), anyString(), any(Role.class), any(Role.class));
		assertNotEquals(roleBeforeUpdate.getCreateDate(), updatedRole.getUpdateDate());

	}

	@Test
	public void getRolePagedResultSetByQuerySucceed() {
		PagedResultSet<Role<Resource>> retrievedRolePagedResultSet = new PagedResultSet<>();
		List<Role<Resource>> roles = givenThatThereAreSavedRoles();
		retrievedRolePagedResultSet.setResults(roles);
		given(roleDao.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(retrievedRolePagedResultSet);

		PagedResultSet<Role<Resource>> rolePagedResultSet = classUnderTest.getRolePagedResultSetBy(new QueryParameters());

		assertNotNull(rolePagedResultSet);
		assertFalse(rolePagedResultSet.getResults().isEmpty());
		verify(roleDao, times(1)).getRolePagedResultSetBy(any(QueryParameters.class));
	}

	@Test
	public void getRolePagedResultSetByQuerySucceedWithEmptyResult() {
		given(roleDao.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(new PagedResultSet<>());

		PagedResultSet<Role<Resource>> rolePagedResultSet = classUnderTest.getRolePagedResultSetBy(new QueryParameters());

		assertNotNull(rolePagedResultSet);
		assertTrue(rolePagedResultSet.getResults().isEmpty());
		verify(roleDao, times(1)).getRolePagedResultSetBy(any(QueryParameters.class));
	}

	@Test
	public void getActiveUserRoleSucceed() {
		UserAccount userAccount = new UserAccount();
		givenThatUserAccountHasGrantedRole(userAccount);
		given(subject.getPrincipal()).willReturn(new UserPrincipal());
		given(userAccountDao.retrieve(any())).willReturn(userAccount);
		given(roleDao.getRole(anyString())).willReturn(new Role<>());
		Role<Resource> activeRole = classUnderTest.getActiveUserRole(ROLE_ID);

		assertNotNull(activeRole);
		verify(roleDao, times(1)).getRole(anyString());
	}

	private void givenThatUserAccountHasGrantedRole(UserAccount userAccount) {
		GrantedRole grantedRole = new GrantedRole();
		grantedRole.setRoleId(ROLE_ID);
		userAccount.getGrantedRoles().add(grantedRole);
	}

	@Test
	public void getActiveUserRoleWithNoActiveAccount() {
		given(subject.getPrincipal()).willReturn(new UserPrincipal());
		assertThrows(UnauthorizedException.class, () -> classUnderTest.getActiveUserRole(ROLE_ID));
	}

	@Test
	public void getActiveUserRoleThatIsNotGranted() {
		given(subject.getPrincipal()).willReturn(new UserPrincipal());
		given(userAccountDao.retrieve(any())).willReturn(new UserAccount());
		assertThrows(UnauthorizedException.class, () -> classUnderTest.getActiveUserRole(ROLE_ID));
	}

	@Test
	public void getRoleSucceed() {
		given(roleDao.getRoleByObjectId(anyString())).willReturn(new Role<>());
		Role role = classUnderTest.getRole(ROLE_ID);

		verify(roleDao, times(1)).getRoleByObjectId(anyString());
		assertNotNull(role);
	}

	@Test
	public void getRoleSucceedWithNullResult() {
		given(roleDao.getRoleByObjectId(anyString())).willReturn(null);
		Role role = classUnderTest.getRole(ROLE_ID);
		verify(roleDao, times(1)).getRoleByObjectId(anyString());
		assertNull(role);
	}

	@Test
	public void saveRoleForRider() {
		ArgumentCaptor<Role<Resource>> roleArgumentCaptor = ArgumentCaptor.forClass(Role.class);
		Role<Resource> roleToBeSaved = new Role<>();
		roleToBeSaved.getOtherProperties().put(FOR_RIDER, true);
		Role<Resource> savedRole = new Role<>();
		savedRole.setId(ID);
		given(roleDao.save(any(Role.class))).willReturn(savedRole);
		classUnderTest.save(roleToBeSaved);
		verify(roleDao, times(1)).save(roleArgumentCaptor.capture());
		savedRole = roleArgumentCaptor.getValue();
		assertTrue((boolean)savedRole.getOtherProperties().get(FOR_RIDER));
	}
}