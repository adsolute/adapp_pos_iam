package com.adapp.adapp_pos_iam.services;

import com.adapp.adapp_pos_models.security.UserDTO;
import com.adapp.security.dao.UserDao;
import com.adapp.security.models.User;
import com.adapp.security.utils.HashingUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	static final String USERNAME = "username";
	static final String PASSWORD = "password";
	@InjectMocks
	UserService classUnderTest;

	@Mock
	UserDao userDao;

	@Test
	void saveSucceed() {
		User user = new User();

		givenThatUserHasUsernameAndPassword(user);
		classUnderTest.save(user);
		ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
		verify(userDao, times(1)).exists(anyString());
		verify(userDao, times(1)).save(userArgumentCaptor.capture());
		User savedUser = userArgumentCaptor.getValue();
		assertNotNull(savedUser.getUserId());
		assertNotNull(savedUser.getSalt());
		assertEquals(HashingUtility.hashPassword(PASSWORD, savedUser.getSalt()), savedUser.getPassword());
	}

	private void givenThatUserHasUsernameAndPassword(User user) {
		user.setUsername(USERNAME);
		user.setPassword(PASSWORD);
	}

	@Test
	void saveFailedUsernameAlreadyExist() {
		User existingUser = new User();

		givenThatUserAlreadyExist(existingUser);
		when(userDao.exists(anyString())).thenReturn(true);
		assertThrows(IllegalArgumentException.class, () -> classUnderTest.save(existingUser));
	}

	private void givenThatUserAlreadyExist(User existingUser) {
		existingUser.setUsername("existing username");
		existingUser.setId("existing id");
		existingUser.setUserId("existing user id");
	}

	@Test
	void saveFailedNoUsername() {
		User userWithNoUsername = new User();
		assertThrows(IllegalArgumentException.class, () -> classUnderTest.save(userWithNoUsername));
	}

	@Test
	void saveFailedNoPassword() {
		User userWithNoPassword = new User();
		userWithNoPassword.setUsername(USERNAME);
		assertThrows(IllegalArgumentException.class, () -> classUnderTest.save(userWithNoPassword));
	}

	@Test
	void updateSucceed() {
		User user = new User();
		ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

		givenThatUserAlreadyExist(user);
		given(userDao.getByUsername(anyString())).willReturn(user);
		classUnderTest.update(user);
		verify(userDao, times(1)).save(userArgumentCaptor.capture());
		User updatedUser = userArgumentCaptor.getValue();
		assertNotNull(updatedUser.getId());
		assertNotNull(updatedUser.getUserId());
		assertNotNull(updatedUser.getUsername());
	}

	@Test
	void updateFailedUsernameIsNull() {
		User userWithNoUsername = new User();
		assertThrows(IllegalArgumentException.class, () -> classUnderTest.update(userWithNoUsername));
	}

	@Test
	void updateFailedIdIsNull() {
		User userWithNoId = new User();
		userWithNoId.setUsername("username");
		assertThrows(IllegalArgumentException.class, () -> classUnderTest.update(userWithNoId));
	}

	@Test
	void getUserDTOByUsernameSucceed() {
		User retrievedUser = new User();
		retrievedUser.setUsername(USERNAME);
		given(userDao.getByUsername(anyString())).willReturn(retrievedUser);
		UserDTO retrievedUserDTO = classUnderTest.getUserDTOByUsername(USERNAME);
		retrievedUserDTO.getUsername();
		assertEquals(retrievedUser.getUsername(), retrievedUserDTO.getUsername());
	}

	@Test
	void getUserDTOByUsernameNotFound() {
		assertThrows(NotFoundException.class, () -> classUnderTest.getUserDTOByUsername(USERNAME));
	}
}