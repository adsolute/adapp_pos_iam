package com.adapp.adapp_pos_iam.services;

import com.adapp.adapp_pos_iam.dao.UserAccountDao;
import com.adapp.adapp_pos_iam.utils.IAMKafkaAuditSender;
import com.adapp.adapp_pos_models.ContactInformation;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.profiles.UserAccountDTO;
import com.adapp.security.dao.BaseDao.PagedResultSet;
import com.adapp.security.dao.BaseDao.QueryParameters;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.dao.UserDao;
import com.adapp.security.models.Role;
import com.adapp.security.models.User;
import com.adapp.security.models.UserPrincipal;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.apache.shiro.util.ThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserAccountServiceTest {

	public static final String USERNAME = "username";
	public static final String USER_ACCOUNT_ID = "userAccountId";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";
	public static final String ID = "id";
	@InjectMocks
	UserAccountService classUnderTest;

	@Mock
	UserAccountDao userAccountDao;

	@Mock
	UserDao userDao;

	@Mock
	Subject subject;

	@Mock
	RoleDao<Resource> roleDao;

	@Mock
	IAMKafkaAuditSender<UserAccount> iamKafkaAuditSender;

	@BeforeEach
	void setup() {
		ThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	void saveSucceed() {
		UserAccount savedUserAccount = new UserAccount();
		savedUserAccount.setId(ID);
		given(userAccountDao.save(any(UserAccount.class))).willReturn(savedUserAccount);
		classUnderTest.save(new UserAccount());
		ArgumentCaptor<UserAccount> userAccountArgumentCaptor = ArgumentCaptor.forClass(UserAccount.class);
		verify(userAccountDao, times(1)).save(userAccountArgumentCaptor.capture());
		verify(iamKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), eq(null), anyString(), anyString());
		savedUserAccount = userAccountArgumentCaptor.getValue();
		assertNotNull(savedUserAccount);
		assertNotNull(savedUserAccount.getAccountId());
	}

	@Test
	void saveFailed() {
		UserAccount userAccount = new UserAccount();
		willThrow(MongoTimeoutException.class).given(userAccountDao).save(any(UserAccount.class));
		assertThrows(MongoException.class, () -> classUnderTest.save(userAccount));
	}

	@Test
	void updateSucceed() {
		UserAccount userAccount = givenThatUserHasAccount();
		ArgumentCaptor<UserAccount> userAccountArgumentCaptor = ArgumentCaptor.forClass(UserAccount.class);
		given(userAccountDao.findAndReplace(anyString(), any(UserAccount.class))).willReturn(new UserAccount());

		classUnderTest.update(userAccount);

		verify(userAccountDao, times(1)).findAndReplace(anyString(), userAccountArgumentCaptor.capture());
		verify(iamKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), eq(null), anyString(), anyString(), any(UserAccount.class), any(UserAccount.class));
		UserAccount savedUserAccount = userAccountArgumentCaptor.getValue();
		assertNotNull(savedUserAccount);
		assertNotNull(savedUserAccount.getAccountId());
	}

	@Test
	void updateFailedBadRequest() {
		UserAccount userAccountToUpdate = new UserAccount();
		assertThrows(BadRequestException.class, () -> classUnderTest.update(userAccountToUpdate));
	}

	@Test
	void getUserAccountDTOPagedResultSetByQuerySucceed() {
		PagedResultSet<User> retrievedPagedResultSet = givenThatUsersAreRetrieved();
		UserAccount userAccount = givenThatUserHasAccount();
		given(userDao.getPagedResultSetBy(any(QueryParameters.class))).willReturn(retrievedPagedResultSet);
		given(userAccountDao.retrieve(anyString())).willReturn(userAccount);

		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = classUnderTest.getUserAccountDTOPagedResultSetBy(new QueryParameters());
		UserAccountDTO userAccountDTO = userAccountDTOPagedResultSet.getResults().get(0);

		verify(userDao, times(1)).getPagedResultSetBy(any(QueryParameters.class));
		verify(userAccountDao, times(1)).retrieve(anyString());
		assertEquals(retrievedPagedResultSet.getPage(), userAccountDTOPagedResultSet.getPage());
		assertEquals(retrievedPagedResultSet.getSize(), userAccountDTOPagedResultSet.getSize());
		assertEquals(retrievedPagedResultSet.getTotalCount(), userAccountDTOPagedResultSet.getTotalCount());
		assertFalse(userAccountDTOPagedResultSet.getResults().isEmpty());
		assertEquals(USERNAME, userAccountDTO.getUsername());
		assertEquals(userAccount.getContactInformation().getEmailAddress(), userAccountDTO.getEmail());
		assertEquals(userAccount.getContactInformation().getPhoneNumber(), userAccountDTO.getPhone());
	}

	private PagedResultSet<User> givenThatUsersAreRetrieved() {
		User user = new User();
		user.setUsername(USERNAME);
		user.setAccountId(USER_ACCOUNT_ID);
		PagedResultSet<User> userPagedResultSet = new PagedResultSet<>();
		userPagedResultSet.setResults(Collections.singletonList(user));
		return userPagedResultSet;
	}

	private UserAccount givenThatUserHasAccount() {
		UserAccount userAccount = new UserAccount();
		userAccount.setId(ID);
		userAccount.setAccountId(USER_ACCOUNT_ID);
		userAccount.setContactInformation(new ContactInformation());
		userAccount.getContactInformation().setPhoneNumber(PHONE);
		userAccount.getContactInformation().setEmailAddress(EMAIL);
		return userAccount;
	}

	@Test
	void getUserAccountDTOPagedResultSetByQuerySucceedWithNoAccount() {
		PagedResultSet<User> retrievedPagedResultSet = givenThatUsersAreRetrieved();
		given(userDao.getPagedResultSetBy(any(QueryParameters.class))).willReturn(retrievedPagedResultSet);
		given(userAccountDao.retrieve(anyString())).willReturn(null);

		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = classUnderTest.getUserAccountDTOPagedResultSetBy(new QueryParameters());
		UserAccountDTO userAccountDTO = userAccountDTOPagedResultSet.getResults().get(0);

		verify(userDao, times(1)).getPagedResultSetBy(any(QueryParameters.class));
		verify(userAccountDao, times(1)).retrieve(anyString());
		assertEquals(retrievedPagedResultSet.getPage(), userAccountDTOPagedResultSet.getPage());
		assertEquals(retrievedPagedResultSet.getSize(), userAccountDTOPagedResultSet.getSize());
		assertEquals(retrievedPagedResultSet.getTotalCount(), userAccountDTOPagedResultSet.getTotalCount());
		assertFalse(userAccountDTOPagedResultSet.getResults().isEmpty());
		assertEquals(USERNAME, userAccountDTO.getUsername());
		assertNull(userAccountDTO.getEmail());
		assertNull(userAccountDTO.getPhone());
	}

	@Test
	void getUserAccountDTOPagedResultSetByQuerySucceedWithEmptyResults() {
		given(userDao.getPagedResultSetBy(any(QueryParameters.class))).willReturn(new PagedResultSet<>());

		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = classUnderTest.getUserAccountDTOPagedResultSetBy(new QueryParameters());

		verify(userDao, times(1)).getPagedResultSetBy(any(QueryParameters.class));
		verify(userAccountDao, times(0)).retrieve(anyString());
		assertTrue(userAccountDTOPagedResultSet.getResults().isEmpty());
	}

	@Test
	void getUserAccountByIdOrSubjectSucceed() {
		classUnderTest.getUserAccountByIdOrSubject(USER_ACCOUNT_ID);
		verify(userAccountDao, times(1)).retrieve(anyString());
	}

	@Test
	void getUserAccountByIdUsingSubjectSucceed() {
		UserPrincipal userPrincipal = new UserPrincipal();
		userPrincipal.setAccountId(USER_ACCOUNT_ID);
		given(subject.getPrincipal()).willReturn(userPrincipal);
		classUnderTest.getUserAccountByIdOrSubject(null);
		verify(userAccountDao, times(1)).retrieve(anyString());
	}

	@Test
	void getUserAccountByAccountIdSucceed() {
		classUnderTest.getUserAccountByAccountId(USER_ACCOUNT_ID);
		verify(userAccountDao, times(1)).retrieve(anyString());
	}

	@Test
	void getUserAccountByAccountIdBadRequest() {
		assertThrows(BadRequestException.class, () -> classUnderTest.getUserAccountByAccountId(null));
	}

	@Test
	void deleteSucceed() {
		User user = new User();
		user.setAccountId(USER_ACCOUNT_ID);
		given(userDao.getByUsername(anyString())).willReturn(user);
		given(userAccountDao.retrieve(anyString())).willReturn(new UserAccount());
		classUnderTest.delete(USERNAME);
		verify(userDao, times(1)).remove(any(User.class));
		verify(userAccountDao, times(1)).remove(any(UserAccount.class));
		verify(iamKafkaAuditSender, times(1)).send(any(Resource.class), eq(null), anyString(), anyString());
	}

	@Test
	void deleteNotFound() {
		given(userDao.getByUsername(anyString())).willReturn(null);
		assertThrows(NotFoundException.class, () -> classUnderTest.delete(USERNAME));
	}

	@Test
	void getRiderUserAccountsSucceed() {
		PagedResultSet<Role<Resource>> riderAccountPagedResultSet = givenThatRiderRolesWereCreated();
		given(roleDao.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(riderAccountPagedResultSet);
		given(userAccountDao.getUserAccountsWithRolesInListByBranch(anyList(), anyString())).willReturn(Arrays.asList(new UserAccount()));
		List<UserAccount> riderUserAccounts = classUnderTest.getRiderUserAccounts("branchId");
		assertFalse(riderUserAccounts.isEmpty());
	}

	private PagedResultSet<Role<Resource>> givenThatRiderRolesWereCreated() {
		PagedResultSet<Role<Resource>> riderAccountPagedResultSet = new PagedResultSet<>();
		Role<Resource> riderRole = new Role<Resource>();
		riderRole.setId("roleId");
		riderAccountPagedResultSet.getResults().add(riderRole);
		return riderAccountPagedResultSet;
	}

	@Test
	void getRiderUserAccountsSucceedButNoRiderRolesExist() {
		given(roleDao.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(new PagedResultSet());
		List<UserAccount> riderUserAccounts = classUnderTest.getRiderUserAccounts("branchId");
		assertTrue(riderUserAccounts.isEmpty());
	}

	@Test
	void getRiderUserAccountsSucceedButNoRiderRoleIsAssigned() {
		PagedResultSet<Role<Resource>> riderAccountPagedResultSet = givenThatRiderRolesWereCreated();
		given(roleDao.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(riderAccountPagedResultSet);
		given(userAccountDao.getUserAccountsWithRolesInListByBranch(anyList(), anyString())).willReturn(Collections.emptyList());
		List<UserAccount> riderUserAccounts = classUnderTest.getRiderUserAccounts("branchId");
		assertTrue(riderUserAccounts.isEmpty());
	}
}