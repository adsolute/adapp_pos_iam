package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.services.UserAccountService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.profiles.UserAccountDTO;
import com.adapp.security.dao.BaseDao.PagedResultSet;
import com.adapp.security.dao.BaseDao.QueryParameters;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserAccountControllerTest {

	private static final String BRANCH_ID = "branchId";
	private static final String ACCOUNT_ID = "accountId";
	static final String USERNAME = "username";
	@InjectMocks
	UserAccountController classUnderTest;

	@Mock
	UserAccountService userAccountService;

	@Mock
	Subject subject;

	@BeforeEach
	void setup() {
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	void saveSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(userAccountService.save(any(UserAccount.class))).willReturn(new UserAccount());
		Response response = classUnderTest.save(BRANCH_ID, new UserAccount());
		UserAccount savedUserAccount = (UserAccount) response.getEntity();
		verify(userAccountService, times(1)).save(any(UserAccount.class));
		assertEquals(200, response.getStatus());
		assertNotNull(savedUserAccount);
	}

	@Test
	void saveFailedNotPermitted() {
		Response response = classUnderTest.save(BRANCH_ID, new UserAccount());
		assertEquals(401, response.getStatus());
	}

	@Test
	void saveFailed() {
		UserAccount userAccount = new UserAccount();
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NullPointerException.class).given(userAccountService).save(any(UserAccount.class));
		assertThrows(ServerException.class, () -> classUnderTest.save(BRANCH_ID, userAccount));
	}

	@Test
	void updateSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.update(BRANCH_ID, new UserAccount());
		assertEquals(200, response.getStatus());
	}

	@Test
	void updateFailedHasNoAccountId() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(BadRequestException.class).given(userAccountService).update(any(UserAccount.class));

		Response response = classUnderTest.update(BRANCH_ID, new UserAccount());
		assertEquals(400, response.getStatus());
	}

	@Test
	void updateNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.update(BRANCH_ID, new UserAccount());
		assertEquals(401, response.getStatus());
	}

	@Test
	void updateFailed() {
		UserAccount userAccount = new UserAccount();
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(userAccountService).update(any(UserAccount.class));

		assertThrows(ServerException.class, () -> classUnderTest.update(BRANCH_ID, userAccount));
	}

	@Test
	void getPagedResultSetSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		PagedResultSet<UserAccountDTO> retrievedPagedResultSet = givenThatUserAccountsWereRetrieved();
		given(userAccountService.getUserAccountDTOPagedResultSetBy(any(QueryParameters.class))).willReturn(retrievedPagedResultSet);

		Response response = classUnderTest.getPagedResultSet(BRANCH_ID, new QueryParameters());
		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = (PagedResultSet<UserAccountDTO>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(userAccountDTOPagedResultSet);
		assertFalse(userAccountDTOPagedResultSet.getResults().isEmpty());
		verify(userAccountService, times(1)).getUserAccountDTOPagedResultSetBy(any(QueryParameters.class));
	}

	private PagedResultSet<UserAccountDTO> givenThatUserAccountsWereRetrieved() {
		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = new PagedResultSet<>();
		userAccountDTOPagedResultSet.setResults(Collections.singletonList(new UserAccountDTO()));
		return userAccountDTOPagedResultSet;
	}

	@Test
	void getPagedResultSetSucceedWithEmptyResults() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(userAccountService.getUserAccountDTOPagedResultSetBy(any(QueryParameters.class))).willReturn(new PagedResultSet<>());

		Response response = classUnderTest.getPagedResultSet(BRANCH_ID, new QueryParameters());
		PagedResultSet<UserAccountDTO> userAccountDTOPagedResultSet = (PagedResultSet<UserAccountDTO>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(userAccountDTOPagedResultSet);
		assertTrue(userAccountDTOPagedResultSet.getResults().isEmpty());
		verify(userAccountService, times(1)).getUserAccountDTOPagedResultSetBy(any(QueryParameters.class));
	}

	@Test
	void getPagedResultSetFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.getPagedResultSet(BRANCH_ID, new QueryParameters());

		assertEquals(401, response.getStatus());
	}

	@Test
	void getPagedResultSetFailed() {
		QueryParameters queryParameters = new QueryParameters();
		given(subject.isPermitted(anyString())).willReturn(true);
		given(userAccountService.getUserAccountDTOPagedResultSetBy(any(QueryParameters.class))).willThrow(NullPointerException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getPagedResultSet(BRANCH_ID, queryParameters));
	}

	@Test
	void getAccountByIdSucceed() {
		classUnderTest.getAccountById(ACCOUNT_ID);
		verify(userAccountService, times(1)).getUserAccountByIdOrSubject(anyString());
	}

	@Test
	void getAccountByIdFailed() {
		given(userAccountService.getUserAccountByIdOrSubject(anyString())).willThrow(NullPointerException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getAccountById(ACCOUNT_ID));
		verify(userAccountService, times(1)).getUserAccountByIdOrSubject(anyString());
	}

	@Test
	void getAccountByAccountIdSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getAccountByAccountId(ACCOUNT_ID, BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	void getAccountByAccountIdFailedUnauthorized() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.getAccountByAccountId(ACCOUNT_ID, BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	void getAccountByAccountIdFailedBadRequest() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(userAccountService.getUserAccountByAccountId(anyString())).willThrow(BadRequestException.class);
		Response response = classUnderTest.getAccountByAccountId(ACCOUNT_ID, BRANCH_ID);
		assertEquals(400, response.getStatus());
	}

	@Test
	void getAccountByAccountIdFailedSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(userAccountService.getUserAccountByAccountId(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getAccountByAccountId(ACCOUNT_ID, BRANCH_ID));
	}

	@Test
	void deleteSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.delete(USERNAME, BRANCH_ID);
		assertEquals(200, response.getStatus());
		verify(userAccountService, times(1)).delete(anyString());

	}

	@Test
	void deleteNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.delete(USERNAME, BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	void deleteNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotFoundException.class).given(userAccountService).delete(anyString());
		Response response = classUnderTest.delete(USERNAME, BRANCH_ID);
		assertEquals(404, response.getStatus());
	}

	@Test
	void deleteSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(userAccountService).delete(anyString());
		assertThrows(ServerException.class, () -> classUnderTest.delete(USERNAME, BRANCH_ID));
	}

	@Test
	void getRiderAccountsSucceed() {
		Response response = classUnderTest.getRiderAccounts(BRANCH_ID);
		assertEquals(200, response.getStatus());
		verify(userAccountService, times(1)).getRiderUserAccounts(anyString());
	}

	@Test
	void getRiderAccountsFailed() {
		given(userAccountService.getRiderUserAccounts(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getRiderAccounts(BRANCH_ID));
	}
}