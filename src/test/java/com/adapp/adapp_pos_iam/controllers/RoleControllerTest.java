package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.services.RoleService;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.security.dao.BaseDao.PagedResultSet;
import com.adapp.security.dao.BaseDao.QueryParameters;
import com.adapp.security.models.Role;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class RoleControllerTest {

	static final String BRANCH_ID = "branchId";
	static final String ROLE_ID = "role id";
	@InjectMocks
	RoleController classUnderTest;

	@Mock
	RoleService roleService;

	@Mock
	Subject subject;

	@BeforeEach
	void setup() {
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	void getAllRolesSucceed() {

		List<Role<Resource>> savedRoles = givenThatThereAreSavedRoles();
		when(roleService.getAllRoles()).thenReturn(savedRoles);
		Response response = classUnderTest.getAllRoles();
		List<Role> roles =(List<Role>) response.getEntity();
		assertEquals(200, response.getStatus());
		assertTrue(!roles.isEmpty());
		verify(roleService, times(1)).getAllRoles();
	}

	private List<Role<Resource>> givenThatThereAreSavedRoles() {
		return Arrays.asList(new Role());
	}

	@Test
	void getAllRolesSucceedWithEmptyList() {
		Response response = classUnderTest.getAllRoles();
		List<Role> roles =(List<Role>) response.getEntity();
		assertEquals(200, response.getStatus());
		assertTrue(roles.isEmpty());
		verify(roleService, times(1)).getAllRoles();
	}

	@Test
	void getAllRolesFailed() {
		willThrow(MongoTimeoutException.class).given(roleService).getAllRoles();
		assertThrows(ServerException.class, () -> classUnderTest.getAllRoles());
	}

	@Test
	void saveSucceed() {
		willDoNothing().given(roleService).save(any());
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.save(new Role<>(), BRANCH_ID);
		assertEquals(200, response.getStatus());
		verify(roleService, times(1)).save(any());
	}

	@Test
	void saveNotPermitted() {
		Response response = classUnderTest.save(new Role<>(), BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	void saveFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(MongoTimeoutException.class).given(roleService).save(any());
		assertThrows(ServerException.class, () -> classUnderTest.save(new Role<>(), BRANCH_ID));
	}

	@Test
	void getRolePagedResultSetByQuerySucceed() {
		PagedResultSet<Role<Resource>> retrievedRolePagedResultSet = new PagedResultSet<>();
		List<Role<Resource>> roles = givenThatThereAreSavedRoles();
		retrievedRolePagedResultSet.setResults(roles);
		given(roleService.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(retrievedRolePagedResultSet);
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.getRolePagedResultSet(BRANCH_ID, new QueryParameters());
		PagedResultSet<Role> rolePagedResultSet = (PagedResultSet<Role>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(rolePagedResultSet);
		assertFalse(rolePagedResultSet.getResults().isEmpty());
	}

	@Test
	void getRolePagedResultSetByQuerySucceedWithEmptyResults() {
		given(roleService.getRolePagedResultSetBy(any(QueryParameters.class))).willReturn(new PagedResultSet<>());
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.getRolePagedResultSet(BRANCH_ID, new QueryParameters());
		PagedResultSet<Role> rolePagedResultSet = (PagedResultSet<Role>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(rolePagedResultSet);
		assertTrue(rolePagedResultSet.getResults().isEmpty());
	}

	@Test
	void getRoleResultSetByQueryFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.getRolePagedResultSet(BRANCH_ID, new QueryParameters());

		assertEquals(401, response.getStatus());
	}

	@Test
	void getRoleResultSetByQueryFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(roleService.getRolePagedResultSetBy(any(QueryParameters.class))).willThrow(NullPointerException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getRolePagedResultSet(BRANCH_ID, new QueryParameters()));
	}

	@Test
	void getActiveUserRoleSucceed() {
		given(roleService.getActiveUserRole(anyString())).willReturn(new Role<>());

		Response response = classUnderTest.getActiveUserRole(ROLE_ID);

		assertEquals(200, response.getStatus());
		assertNotNull(response.getEntity());
		verify(roleService, times(1)).getActiveUserRole(anyString());
	}

	@Test
	void getActiveUserRoleNotFound() {

		Response response = classUnderTest.getActiveUserRole(ROLE_ID);

		assertEquals(404, response.getStatus());
		verify(roleService, times(1)).getActiveUserRole(anyString());
	}

	@Test
	void getActiveUserSomethingWentWront() {
		given(roleService.getActiveUserRole(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getActiveUserRole(ROLE_ID));
	}

	@Test
	void getActiveUserRoleNotGranted() {
		given(roleService.getActiveUserRole(anyString())).willThrow(UnauthorizedException.class);
		Response response = classUnderTest.getActiveUserRole(ROLE_ID);

		assertEquals(401, response.getStatus());
		assertTrue(response.getEntity().toString().contains(ROLE_ID));
	}


	@Test
	void getRoleSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(roleService.getRole(anyString())).willReturn(new Role<>());
		Response response = classUnderTest.getRole(ROLE_ID, BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	void getRoleFailedNotPermitted() {
		Response response = classUnderTest.getRole(ROLE_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
	}

	@Test
	void getRoleFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getRole(ROLE_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
	}

	@Test
	void getRoleFailedIllegalArgument() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(roleService.getRole(anyString())).willThrow(IllegalArgumentException.class);
		Response response = classUnderTest.getRole(ROLE_ID, BRANCH_ID);

		assertEquals(400, response.getStatus());
	}


	@Test
	void getRoleSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(roleService.getRole(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getRole(ROLE_ID, BRANCH_ID));
	}

	@Test
	void updateRoleSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Role roleToUpdate = new Role();
		givenThatRoleHasObjectId(roleToUpdate);
		Response response = classUnderTest.update(BRANCH_ID, roleToUpdate);

		assertEquals(200, response.getStatus());
		verify(roleService, times(1)).save(any(Role.class));
	}

	private void givenThatRoleHasObjectId(Role role) {
		role.setId(ROLE_ID);
	}

	@Test
	void updateFailedNullObjectId() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.update(BRANCH_ID, new Role<>());

		assertEquals(400, response.getStatus());
	}

	@Test
	void updateNotPermitted() {
		Response response = classUnderTest.update(BRANCH_ID, new Role<>());
		assertEquals(401, response.getStatus());
	}

	@Test
	void updateSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(roleService).save(any(Role.class));
		Role roleToUpdate = new Role();
		givenThatRoleHasObjectId(roleToUpdate);
		assertThrows(ServerException.class, () -> classUnderTest.update(BRANCH_ID, roleToUpdate));
	}
}