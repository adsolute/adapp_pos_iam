package com.adapp.adapp_pos_iam.controllers;


import com.adapp.adapp_pos_iam.dao.UserAccountDao;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.profiles.UserAccount;
import com.adapp.adapp_pos_models.security.Credentials;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.powermock.api.mockito.PowerMockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthControllerTest {

    @InjectMocks
    AuthController classUnderTest;

    @Mock
    UserAccountDao userAccountDao;

    @Mock
    Subject subject;

    @BeforeEach
    void setup() {
        SubjectThreadState threadState = new SubjectThreadState(subject);
        threadState.bind();
    }

    @Test
    void loginSuccessful() {
        Credentials credentials = new Credentials();
        givenThatCredentialsAreValid(credentials);
        willDoNothing().given(subject).login(any(UsernamePasswordToken.class));
        when(subject.getPrincipal()).thenReturn(new UserPrincipal());
        when(userAccountDao.retrieve(any())).thenReturn(new UserAccount());

        Response response = classUnderTest.login(credentials);

        assertEquals(200, response.getStatus());
        assertNotNull(response.getEntity());
    }

    @Test
    void loginSuccessfulButUnauthorized() {
        Credentials credentials = new Credentials();
        givenThatCredentialsAreValid(credentials);
        willDoNothing().given(subject).login(any(UsernamePasswordToken.class));
        when(subject.getPrincipal()).thenReturn(new UserPrincipal());
        when(userAccountDao.retrieve(any())).thenReturn(null);

        Response response = classUnderTest.login(credentials);

        assertEquals(401, response.getStatus());
        assertNotNull(response.getEntity());
        assertTrue(response.getEntity().toString().contains("Please Contact Admin"));
    }

    @Test
    void loginFailedIncorrectCredentials() {
        Credentials credentials = new Credentials();
        givenThatCredentialsAreValid(credentials);
        willThrow(IncorrectCredentialsException.class).given(subject).login(any(UsernamePasswordToken.class));

        Response response = classUnderTest.login(credentials);

        assertEquals(401, response.getStatus());
        assertEquals(AuthController.INVALID_CREDENTIALS, response.getEntity());
    }

    @Test
    void loginFailedSomethingWentWrong() {
        Credentials credentials = new Credentials();
        givenThatCredentialsAreValid(credentials);
        willThrow(RuntimeException.class).given(subject).login(any(UsernamePasswordToken.class));
        assertThrows(ServerException.class, () -> classUnderTest.login(credentials));
    }

    private void givenThatCredentialsAreValid(Credentials credentials) {
        credentials.setUsername("username");
        credentials.setPassword("password");
    }

    @Test
    void checkAuthenticationSucceed() {
        given(subject.isAuthenticated()).willReturn(true);
        Response response = classUnderTest.checkAuthentication();
        assertEquals(200, response.getStatus());
    }

    @Test
    void checkAuthenticationFailedUnauthorized() {
        given(subject.isAuthenticated()).willReturn(false);
        Response response = classUnderTest.checkAuthentication();
        assertEquals(401, response.getStatus());
    }

    @Test
    void logoutSuccess() {
        Response response = classUnderTest.logout();
        assertEquals(200, response.getStatus());
    }

    @Test
    void logoutFailed() {
        willThrow(RuntimeException.class).given(subject).logout();
        assertThrows(ServerException.class, () -> classUnderTest.logout());
    }
}
