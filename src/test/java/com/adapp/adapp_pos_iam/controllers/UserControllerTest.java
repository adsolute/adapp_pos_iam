package com.adapp.adapp_pos_iam.controllers;

import com.adapp.adapp_pos_iam.services.UserService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.security.models.User;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

	static final String BRANCH_ID = "branchId";
	static final String USERNAME = "username";
	@InjectMocks
	UserController classUnderTest;

	@Mock
	UserService userService;

	Subject mockSubject;

	@BeforeEach
	void setup() {
		mockSubject = mock(Subject.class);
		SubjectThreadState threadState = new SubjectThreadState(mockSubject);
		threadState.bind();
	}

	@Test
	void getUserByAccountIdSucceed() {
		Response response = classUnderTest.getUserByAccountId(USERNAME, BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	void getUserByAccountIdNotFound() {
		given(userService.getUserDTOByUsername(anyString())).willThrow(NotFoundException.class);
		Response response = classUnderTest.getUserByAccountId(USERNAME, BRANCH_ID);
		assertEquals(404, response.getStatus());
	}

	@Test
	void getUserByAccountIdSomethingWentWrong() {
		given(userService.getUserDTOByUsername(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getUserByAccountId(USERNAME, BRANCH_ID));
	}

	@Test
	void saveSucceed() {
		when(mockSubject.isPermitted(anyString())).thenReturn(true);
		given(userService.save(any(User.class))).willReturn(new User());
		Response response = classUnderTest.save(new User(), BRANCH_ID);
		User createdUser = (User) response.getEntity();

		verify(userService, times(1)).save(any(User.class));
		assertEquals(200, response.getStatus());
		assertNotNull(createdUser);
	}

	@Test
	void saveFailedNotPermitted() {
		Response response = classUnderTest.save(new User(), BRANCH_ID);
		verify(mockSubject, times(1)).isPermitted(anyString());
		assertEquals(401, response.getStatus());
	}

	@Test
	void saveFailed() {
		User user = new User();
		when(mockSubject.isPermitted(anyString())).thenReturn(true);
		willThrow(IllegalArgumentException.class).given(userService).save(any(User.class));
		assertThrows(ServerException.class, () -> classUnderTest.save(user, BRANCH_ID));
	}

	@Test
	void updateSucceed() {
		when(mockSubject.isPermitted(anyString())).thenReturn(true);
		Response response = classUnderTest.update(new User(), BRANCH_ID);
		verify(userService, times(1)).update(any(User.class));
		assertEquals(200, response.getStatus());
	}

	@Test
	void updateFailedNotPermitted() {
		Response response = classUnderTest.update(new User(), BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	void updateFailed() {
		User user = new User();
		when(mockSubject.isPermitted(anyString())).thenReturn(true);
		willThrow(IllegalArgumentException.class).given(userService).update(any(User.class));
		assertThrows(ServerException.class, () -> classUnderTest.update(user, BRANCH_ID));
	}
}