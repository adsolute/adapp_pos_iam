FROM openjdk:11
MAINTAINER ssbifoodservice.com
COPY target/adapp-pos-iam-*.jar adapp-pos-iam.jar
ENTRYPOINT ["java","-jar","adapp-pos-iam.jar"]
